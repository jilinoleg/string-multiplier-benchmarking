import sys

name_of_script = sys.argv[0]

if len(sys.argv) == 0:
    print("USAGE: multiplystring \"STRING\" NUMBER")

if sys.argv[1] == "-h":
    print("Simple terminal utility to multiply a string (it is easily done in Python tho) \n USAGE: multiplystring \"STRING\" NUMBER \n -h - Print this screen and exit")
    quit()

if len(sys.argv) != 3:
    print("USAGE: multiplystring \"STRING\" NUMBER")

string = sys.argv[1]
multiplier = int(sys.argv[2])

print(string * multiplier)
