{

  description = "Simple flake with development shell to do experiment on benchmarking my crappy string multiplier apps";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        #packages.hello = pkgs.hello;
        #packages.hyperfine = pgks.hyperfine;
        devShell = pkgs.mkShell { buildInputs = [ pkgs.gcc pkgs.python3 pkgs.hyperfine ]; };
      });
}
