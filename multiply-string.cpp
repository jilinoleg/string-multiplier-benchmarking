#include <iostream>
#include <string>
 #include <unistd.h>

 // https://overflow.lunar.icu/questions/11843226/multiplying-a-string-by-an-int-in-c#62520910
 /* Overloading * operator */
std::string operator * (std::string a, unsigned int b) {
    std::string output = "";
    while (b--) {
        output += a;
    }
    return output;
}

int convert_to_int(std::string number){
    int i = 0;
    for (char c : number) {
        // Checking if the element is number
        if (c >= '0' && c <= '9') {
            i = i * 10 + (c - '0');
        }
        // Otherwise print bad output
        else {
            std::cerr << "Not a number, Usage: \"STRING\" NUMBER" << std::endl;
        }
    }
    return i;
}


int main(int argc, char* argv[])
{
    char c;
     while ((c = getopt (argc, argv, "h")) != -1)
     switch (c)
       {
       case 'h':
         printf(" Simple terminal utility to multiply a string (it is easily done in Python tho) \n USAGE: multiplystring \"STRING\" NUMBER \n -h - Print this screen and exit");
         return 0;
       case '?':
         if (isprint (optopt))
           fprintf (stderr, "Unknown option `-%c'.\n", optopt);
         else
           fprintf (stderr,
                    "Unknown option character `\\x%x'.\n",
                    optopt);
         return 1;
       default:
         abort ();
       }

    // Check the number of parameters
    if (argc < 3) {
        // Tell the user how to run the program
        std::cerr << "Usage: " << argv[0] << " \"STRING\" NUMBER" << std::endl;
        /* "Usage messages" are a conventional way of telling the user
         * how to run a program if they enter the command incorrectly.
         */
        return 1;
    }
    // Print the user's name:
    //std::cout << argv[0] << "says hello, " << argv[1] << "!" << std::endl;

    std::string mult_string = argv[2];
    int multiplier = std::stoi(mult_string);
    //delete[]mult_string;

    std::string output = argv[1];
    output = output * multiplier;
    std::cout << output;

    // std::cout << std::endl;
    return 0;
}
