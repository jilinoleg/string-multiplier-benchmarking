
Small experiment with goal of trying to check:
- If Nix, with Flakes, is a good tool to measure performance of applications (also called benchmarking)
- How same simple apps rewritten in different languages will measure compared to others in terms of performance

It is also my first coding project that contains parsing command line arguments in c/c++

## Progress

Repo is generally ready to use and to modify for using it on (much better) other command line applications

Todo:
- Run benchmarks on different devices and compare results
- Fix performance issue in c version that is only visible in benchmark (c++ version received such update in second commit)

## Explainer

Application being measured is "string multiplier", that is used as `./executable STRING NUMBER` and contains arguments number check or `-h` flag support, both to see usage information. There is currently three version - Python 3, C++ and C

We use nix package manager with flakes support to install dependencies declaratively and lock them to specific version, hyperfine tool for measurement, python3.10 and gcc/g++ for running and building program respectively.

Once you install nix and enable flakes, you only need to:
```
git clone https://codeberg.org/jilinoleg/string-multiplier-benchmarking
cd string-multiplier-benchmarking
nix develop
./autobuild.sh # build c and c++ versions
./benchmark.sh # run predefined benchmark command
```

## Results (prone to changes)

- Thinkpad x270 i7 (with power profile set as balanced in kde plasma)
```
Benchmark 1: ./myltiplystring awaw 9999999
  Time (mean ± σ):     474.6 ms ±  15.4 ms    [User: 464.9 ms, System: 8.8 ms]
  Range (min … max):   460.7 ms … 508.3 ms    10 runs

Benchmark 2: ./myltiplystring++ awaw 9999999
  Time (mean ± σ):     149.1 ms ±   5.2 ms    [User: 83.0 ms, System: 65.3 ms]
  Range (min … max):   140.8 ms … 160.2 ms    20 runs

Benchmark 3: python3 multiply-string.py awaw 9999999
  Time (mean ± σ):     102.2 ms ±   5.1 ms    [User: 48.5 ms, System: 52.9 ms]
  Range (min … max):    90.3 ms … 112.0 ms    27 runs

Summary
  python3 multiply-string.py awaw 9999999 ran
    1.46 ± 0.09 times faster than ./myltiplystring++ awaw 9999999
    4.64 ± 0.28 times faster than ./myltiplystring awaw 9999999
```

## LICENSE
Contents of this repository are MIT licensed, but a lot of code is adapted from examples found on the internet (mostly StackExchange answers) so I cannot even say for sure if it is appropriate for me to license it.


